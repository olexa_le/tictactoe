﻿using System;

namespace TicTacToe.Models
{
    public enum CellState
    {
        Empty,
        X,
        O
    }
}

