﻿using System;
using UIKit;
using Foundation;
using CoreAnimation;
using TicTacToe.Models;
using CoreGraphics;

namespace TicTacToe.Views
{
    [Register ("CellView")]
    public class CellView : UIView
    {
        nfloat CellMargin = 20f;
        CAShapeLayer _circleLayer;
        CAShapeLayer _linesLayer;
        CellState _state;

        public event EventHandler CellPressed;

        public CellView (IntPtr ptr) : base (ptr)
        {
            State = CellState.Empty;
        }

        public override void TouchesBegan (NSSet touches, UIEvent evt)
        {
            base.TouchesBegan (touches, evt);
            var handler = CellPressed;
            if (handler != null)
                handler (this, EventArgs.Empty);
        }

        public CellState State {
            get {
                return _state;
            }
            set {
                _state = value;
                DrawCell (State);
            }
        }

        bool _isHighlited;

        public bool IsHighlited {
            get {
                return _isHighlited;
            }
            set {
                _isHighlited = value;
                UIView.Animate (.4, () => {
                    BackgroundColor = value ? UIColor.White : UIColor.Clear;
                });
            }
        }

        void DrawCell (CellState state)
        {
            HideLayers (_circleLayer, _linesLayer);
            switch (state) {
            case CellState.Empty:
                break;
            case CellState.X:
                _linesLayer = CreateLinesLayer ();
                Layer.AddSublayer (_linesLayer);
                AnimateShape (_linesLayer, .3);
                break;
            case CellState.O:
                _circleLayer = CreateCircleLayer ();
                Layer.AddSublayer (_circleLayer);
                AnimateShape (_circleLayer, .3);
                break;
            default:
                throw new ArgumentOutOfRangeException ();
            }
        }

        void HideLayers (CALayer circleLayer, CALayer linesLayer)
        {
            if (circleLayer != null)
                circleLayer.RemoveFromSuperLayer ();
            if (linesLayer != null)
                linesLayer.RemoveFromSuperLayer ();
        }

        CAShapeLayer CreateCircleLayer ()
        {
            var circlePath = new CGPath ();
            var size = (nfloat)Math.Min (Frame.Size.Width, Frame.Size.Height) / 2 - CellMargin;
            circlePath.AddArc (Frame.Size.Width / 2, Frame.Size.Height / 2, size, 0f, (nfloat)Math.PI * 2, true);
            return CreateShape (circlePath, UIColor.Yellow.CGColor);
        }

        CAShapeLayer CreateLinesLayer ()
        {
            var linesPath = new CGPath ();
            linesPath.AddLines (new[] {
                new CGPoint (CellMargin, CellMargin),
                new CGPoint (Frame.Size.Width - CellMargin, Frame.Size.Height - CellMargin)
            });
            linesPath.AddLines (new[] {
                new CGPoint (Frame.Size.Width - CellMargin, CellMargin),
                new CGPoint (CellMargin, Frame.Size.Height - CellMargin)
            });
            linesPath.CloseSubpath ();
            return CreateShape (linesPath, UIColor.Blue.CGColor);
        }

        CAShapeLayer CreateShape (CGPath path, CGColor color)
        {
            var shapeLayer = new CAShapeLayer ();
            shapeLayer.Path = path;
            shapeLayer.FillColor = UIColor.Clear.CGColor;
            shapeLayer.StrokeColor = color;
            shapeLayer.LineWidth = 2;
            shapeLayer.LineCap = CAShapeLayer.CapRound;
            shapeLayer.StrokeEnd = 0;
            return shapeLayer;
        }

        void AnimateShape (CAShapeLayer circleLayer, double duration)
        {
            var animation = new CABasicAnimation ();
            animation.KeyPath = "strokeEnd";

            animation.Duration = duration;
            animation.From = NSObject.FromObject (0f);
            animation.To = NSObject.FromObject (1f);

            animation.TimingFunction = CAMediaTimingFunction.FromName (CAMediaTimingFunction.EaseInEaseOut);

            circleLayer.StrokeEnd = 1;
            circleLayer.AddAnimation (animation, "animateCircle");
        }
    }
}

