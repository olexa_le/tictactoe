﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace TicTacToe.Views
{
    [Register ("BackgroundView")]
    public class BackgroundView : UIView
    {
        nfloat BackgroundMargin = 10;

        public BackgroundView (IntPtr handle) : base (handle)
        {
        }

        public override void Draw (CGRect rect)
        {
            base.Draw (rect);

            using (var g = UIGraphics.GetCurrentContext ()) {
                g.SetLineWidth (1);
                UIColor.LightGray.SetStroke ();

                using (var path = new CGPath ()) {
                    path.AddLines (new [] {
                        new CGPoint (BackgroundMargin, rect.Height / 3),
                        new CGPoint (rect.Width - BackgroundMargin, rect.Height / 3), 
                    });
                    path.AddLines (new [] {
                        new CGPoint (BackgroundMargin, rect.Height * 2 / 3),
                        new CGPoint (rect.Width - BackgroundMargin, rect.Height * 2 / 3), 
                    });

                    path.AddLines (new [] {
                        new CGPoint (rect.Width / 3, BackgroundMargin),
                        new CGPoint (rect.Width / 3, rect.Height - BackgroundMargin), 
                    });
                    path.AddLines (new [] {
                        new CGPoint (rect.Width * 2 / 3, BackgroundMargin),
                        new CGPoint (rect.Width * 2 / 3, rect.Height - BackgroundMargin), 
                    });

                    path.CloseSubpath ();
                    g.AddPath (path);       
                    g.DrawPath (CGPathDrawingMode.Stroke);
                }
            }
        }
    }
}

