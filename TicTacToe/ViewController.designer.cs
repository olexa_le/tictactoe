// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TicTacToe
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.BackgroundView BoardBackground { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView BottomCenter { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView BottomLeft { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView BottomRight { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView CenterCenter { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView CenterLeft { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView CenterRight { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIBarButtonItem Restart { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView TopCenter { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView TopLeft { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		TicTacToe.Views.CellView TopRight { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (BoardBackground != null) {
				BoardBackground.Dispose ();
				BoardBackground = null;
			}
			if (BottomCenter != null) {
				BottomCenter.Dispose ();
				BottomCenter = null;
			}
			if (BottomLeft != null) {
				BottomLeft.Dispose ();
				BottomLeft = null;
			}
			if (BottomRight != null) {
				BottomRight.Dispose ();
				BottomRight = null;
			}
			if (CenterCenter != null) {
				CenterCenter.Dispose ();
				CenterCenter = null;
			}
			if (CenterLeft != null) {
				CenterLeft.Dispose ();
				CenterLeft = null;
			}
			if (CenterRight != null) {
				CenterRight.Dispose ();
				CenterRight = null;
			}
			if (Restart != null) {
				Restart.Dispose ();
				Restart = null;
			}
			if (TopCenter != null) {
				TopCenter.Dispose ();
				TopCenter = null;
			}
			if (TopLeft != null) {
				TopLeft.Dispose ();
				TopLeft = null;
			}
			if (TopRight != null) {
				TopRight.Dispose ();
				TopRight = null;
			}
		}
	}
}
