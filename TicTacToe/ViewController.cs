﻿using System;

using UIKit;
using ObjCRuntime;
using TicTacToe.Views;
using System.Collections.Generic;

namespace TicTacToe
{
    public partial class ViewController : UIViewController
    {
        IList<CellView> _cells;
        bool _bluePlayerTurn;
        bool _gameOver;

        public ViewController (IntPtr handle) : base (handle)
        {
            _bluePlayerTurn = true;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            _cells = new List<CellView> {
                TopLeft,
                TopCenter,
                TopRight,
                CenterLeft,
                CenterCenter,
                CenterRight,
                BottomLeft,
                BottomCenter,
                BottomRight
            };

            // ios7 layout
            if (RespondsToSelector (new Selector ("edgesForExtendedLayout"))) {
                EdgesForExtendedLayout = UIRectEdge.None;
            }
            AddCellsTapEvent (_cells);
            Restart.Clicked += RestartGame;
        }

        public override void ViewDidLayoutSubviews ()
        {
            base.ViewDidLayoutSubviews ();
            BoardBackground.SetNeedsDisplay ();
            foreach (var cell in _cells) {
                cell.State = cell.State;
            }
        }

        void AddCellsTapEvent (IEnumerable<CellView> cells)
        {
            foreach (var cell in cells) {
                cell.CellPressed += HandleCellPress;
            }
        }

        void HandleCellPress (object sender, EventArgs e)
        {
            var cell = (CellView)sender;
            if (cell.State != TicTacToe.Models.CellState.Empty || _gameOver)
                return;
            
            cell.State = _bluePlayerTurn ? TicTacToe.Models.CellState.X : TicTacToe.Models.CellState.O;
            _gameOver = CheckForWinner (_cells);
            if (_gameOver)
                new UIAlertView (string.Empty, _bluePlayerTurn ? "Blue wins!" : "Yellow wins!", null, "Yeah!", null).Show ();
            
            _bluePlayerTurn = !_bluePlayerTurn;
        }

        void RestartGame (object sender, EventArgs e)
        {
            _bluePlayerTurn = true;
            _gameOver = false;
            foreach (var cell in _cells) {
                cell.State = TicTacToe.Models.CellState.Empty;
                cell.IsHighlited = false;
            }
        }

        bool CheckForWinner (IList<CellView> cells)
        {
            return CheckAndMarkCells (cells [0], cells [1], cells [2])
            || CheckAndMarkCells (cells [3], cells [4], cells [5])
            || CheckAndMarkCells (cells [6], cells [7], cells [8])
            || CheckAndMarkCells (cells [0], cells [3], cells [6])
            || CheckAndMarkCells (cells [1], cells [4], cells [7])
            || CheckAndMarkCells (cells [2], cells [5], cells [8])
            || CheckAndMarkCells (cells [0], cells [4], cells [8])
            || CheckAndMarkCells (cells [2], cells [4], cells [6]);
                
        }

        bool CheckAndMarkCells (CellView cell1, CellView cell2, CellView cell3)
        {
            var check = Check (cell1, cell2, cell3);
            if (check)
                Mark (cell1, cell2, cell3);
            return check;    
        }

        bool Check (CellView cell1, CellView cell2, CellView cell3)
        {
            return cell1.State != TicTacToe.Models.CellState.Empty &&
            cell1.State == cell2.State && cell2.State == cell3.State;
        }

        void Mark (CellView cell1, CellView cell2, CellView cell3)
        {
            cell1.IsHighlited = true;
            cell2.IsHighlited = true;
            cell3.IsHighlited = true;
        }
    }
}

